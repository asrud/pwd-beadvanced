const express = require("express");
const cors = require("cors");
const bearerToken = require("express-bearer-token");

const app = express();
app.use(cors());
app.use(express.json());
app.use(bearerToken());

app.use(express.static("public"));

const PORT = 2000;

app.get("/", (req, res) => {
  res.status(200).send(res);
});

const { userRouters, uploadRouter } = require("./routers");
app.use("/users", userRouters);
app.use("/album", uploadRouter);

app.listen(PORT, () => `server bejalan pada port ${PORT}`);
